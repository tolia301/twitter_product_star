#!flask/bin/python
from flask import (Flask, jsonify, request)
import jsonpickle
from model.twit import Twit
app = Flask(__name__)
twits = []


@app.route('/ping', methods=['GET'])
def ping():
    """
    Метод для проверки работоспособности сервиса.
    Возвращает 'pong' в формате JSON, если сервис работает.
    """
    return jsonify({'response': 'pong'})


@app.route('/twit', methods=['POST'])
def create_twit():
    """
    Создает новое сообщение (twit) и добавляет его в список twits.

    Ожидает JSON с данными: id, body, author.
    Возвращает статус создания сообщения.
    """
    twit_json = request.get_json()
    twit = Twit(twit_json['id'], twit_json['body'], twit_json['author'])
    twits.append(twit)
    print(twits)
    return jsonify({'status': 'success'})


@app.route('/twit', methods=['GET'])
def read_twits():
    """
    Получение всех сообщений (twits).

    Возвращает список всех сообщений в формате JSON.
    """
    return jsonify({'twits': jsonpickle.encode(twits)})


@app.route('/twit/<int:twit_id>', methods=['GET'])
def show_twit(twit_id):
    """
    Получение одного сообщения (twit) по его ID.

    Параметры:
        twit_id (int): ID сообщения, которое необходимо получить.

    Возвращает запрошенное сообщение или ошибку, если сообщение не найдено.
    """
    for twit in twits:
        if twit_id == twit.id:
            return jsonpickle.encode(twit), 404


@app.route('/twit/<int:twit_id>', methods=['PUT'])
def find_and_change_twit(twit_id):
    """
    Обновление сообщения (twit) по его ID.

    Параметры:
        twit_id (int): ID сообщения, которое необходимо обновить.

    Возвращает статус обновления или ошибку, если сообщение не найдено.
    """
    twit_json = request.get_json()
    for twit in twits:
        if twit_id == twit.id:
            twit.body = twit_json['body']
            return jsonify({'status': 'success'}), 404


@app.route('/twit/<int:twit_id>', methods=['DELETE'])
def delete_twit(twit_id):
    """
        Удаление сообщения (twit) по его ID.

        Параметры:
            twit_id (int): ID сообщения, которое необходимо удалить.

        Возвращает статус удаления или ошибку, если сообщение не найдено.
    """
    for twit in twits:
        if twit_id == twit.id:
            twits.remove(twit)
            return jsonify({'status': 'success'}), 404


if __name__ == '__main__':
    app.run(debug=True)
